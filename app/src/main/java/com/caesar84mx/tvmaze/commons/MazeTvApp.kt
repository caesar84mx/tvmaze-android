package com.caesar84mx.tvmaze.commons

import android.app.Application
import com.caesar84mx.tvmaze.commons.data.di.AppModule
import com.caesar84mx.tvmaze.commons.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MazeTvApp: Application(), HasAndroidInjector {
    override fun onCreate() {
        super.onCreate()

        instance = this
        DaggerAppComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .build()
            .inject(this)
    }

    companion object {
        lateinit var instance: MazeTvApp
            private set
    }

    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}