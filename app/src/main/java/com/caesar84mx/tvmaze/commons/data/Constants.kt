package com.caesar84mx.tvmaze.commons.data

object Constants {
    object Shows {
        const val PAGE_LIMIT = 15
    }
}