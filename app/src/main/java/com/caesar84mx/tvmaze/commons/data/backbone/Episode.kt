package com.caesar84mx.tvmaze.commons.data.backbone

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Episode(
    var id: Long = 0,
    var serverId: Long,
    var showId: Long,
    var name: String,
    var number: Int,
    var season: Int,
    var summary: String,
    var posterUrl: String?
): BackboneObject, Parcelable