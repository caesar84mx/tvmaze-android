package com.caesar84mx.tvmaze.commons.data.backbone

import android.os.Parcelable
import com.caesar84mx.tvmaze.commons.data.model.api_entities.Schedule
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Show(
    val id: Long,
    val serverId: Long,
    var name: String,
    var posterUrl: String?,
    var schedule: Schedule,
    var genres: List<String>,
    var summary: String,
    var episodes: List<Episode> = listOf()
): BackboneObject, Parcelable