package com.caesar84mx.tvmaze.commons.data.converters

import androidx.room.TypeConverter
import com.caesar84mx.tvmaze.commons.data.model.api_entities.Schedule
import com.caesar84mx.tvmaze.commons.kotlinextensions.toJson
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class ScheduleConverter {
    @TypeConverter
    @ToJson
    fun toJson(schedule: Schedule): String = schedule.toJson()

    @TypeConverter
    @FromJson
    fun fromJson(scheduleJson: String): Schedule {
        val gson = Gson()
        return gson.fromJson(scheduleJson, object : TypeToken<Schedule>() {}.type)
    }
}