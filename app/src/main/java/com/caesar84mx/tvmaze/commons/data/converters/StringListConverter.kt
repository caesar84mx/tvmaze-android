package com.caesar84mx.tvmaze.commons.data.converters

import androidx.room.TypeConverter

class StringListConverter {
    @TypeConverter
    fun fromStringToList(value: String?): MutableList<String>? {
        return value?.split(", ")?.map { it }?.toMutableList() ?: mutableListOf()
    }

    @TypeConverter
    fun fromListToString(value: MutableList<String>?): String? {
        return value?.joinToString() ?: ""
    }
}