package com.caesar84mx.tvmaze.commons.data.di

import com.caesar84mx.tvmaze.main_screen.di.MainActivityModule
import com.caesar84mx.tvmaze.main_screen.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun bindMainActivity(): MainActivity
}