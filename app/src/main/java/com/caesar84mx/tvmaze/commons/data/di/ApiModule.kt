package com.caesar84mx.tvmaze.commons.data.di

import com.caesar84mx.tvmaze.BuildConfig
import com.caesar84mx.tvmaze.commons.data.networking.ShowsApiClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideShowsClient(): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.SHOWS_HOST_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideMazeTvClient(client: Retrofit): ShowsApiClient = client.create(ShowsApiClient::class.java)
}