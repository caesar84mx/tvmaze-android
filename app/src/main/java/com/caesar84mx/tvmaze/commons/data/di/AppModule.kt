package com.caesar84mx.tvmaze.commons.data.di

import android.content.Context
import com.caesar84mx.tvmaze.commons.MazeTvApp
import dagger.Module
import dagger.Provides

@Module
class AppModule(private var app: MazeTvApp) {
    @Provides
    fun provideContext(): Context {
        return app
    }
}