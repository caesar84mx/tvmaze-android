package com.caesar84mx.tvmaze.commons.data.di

import android.content.Context
import com.caesar84mx.tvmaze.commons.data.persistence.TvMazeDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun providesDatabase(app: Context): TvMazeDatabase = TvMazeDatabase.getDatabase(app.applicationContext)

    @Provides
    @Singleton
    fun providesShowsDao(database: TvMazeDatabase) = database.showsDao()

    @Provides
    @Singleton
    fun providesEpisodesDao(database: TvMazeDatabase) = database.episodesDao()
}