package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity

class EpisodeEntityToEpisodeMapper: Mapper<EpisodeEntity, Episode> {
    override fun map(t: EpisodeEntity): Episode = with(t) {
        Episode(
            id = id,
            serverId = serverId,
            showId = showId,
            name = name,
            number = number,
            season = season,
            summary = summary,
            posterUrl = posterUrl
        )
    }
}