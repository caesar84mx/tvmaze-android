package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity
import com.caesar84mx.tvmaze.commons.kotlinextensions.notTull

class EpisodeRawDataToEpisodeEntityMapper: Mapper<EpisodeRawData, EpisodeEntity> {
    override fun map(t: EpisodeRawData): EpisodeEntity = with(t) {
        EpisodeEntity(
            serverId = id,
            name = name,
            number = number,
            season = season,
            summary = summary.notTull(),
            posterUrl = image?.originalSizePosterUrl ?: image?.mediumSizePosterUrl ?: ""
        )
    }
}