package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.kotlinextensions.notTull
import com.caesar84mx.tvmaze.details_screen.data.model.EpisodeItemUI

class EpisodeToEpisodeUIMapper: Mapper<Episode, EpisodeItemUI> {
    override fun map(t: Episode): EpisodeItemUI = with(t) {
        EpisodeItemUI(
            posterUrl = posterUrl.notTull(),
            id = id
        )
    }
}