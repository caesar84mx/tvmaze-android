package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity

class ShowEntityToShowMapper: Mapper<ShowEntity, Show> {
    override fun map(t: ShowEntity): Show = with(t) {
        Show(
            id = id,
            serverId = serverId,
            name = name,
            posterUrl = posterUrl,
            schedule = schedule,
            genres = genres,
            summary = summary
        )
    }
}