package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity

class ShowRawDataToShowEntityMapper: Mapper<ShowRawData, ShowEntity> {
    override fun map(t: ShowRawData): ShowEntity = with(t) {
        ShowEntity(
            serverId = id,
            name = name,
            posterUrl = image?.originalSizePosterUrl ?: image?.mediumSizePosterUrl ?: "",
            schedule = schedule,
            genres = genres,
            summary = summary
        )
    }
}