package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.kotlinextensions.notTull
import com.caesar84mx.tvmaze.main_screen.data.model.ShowsListItemUI

class ShowToShowListItemUiMapper: Mapper<Show, ShowsListItemUI> {
    override fun map(t: Show): ShowsListItemUI = with(t) {
        ShowsListItemUI(id = id, title = name, posterUrl = posterUrl.notTull())
    }
}