package com.caesar84mx.tvmaze.commons.data.mappers

import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.Mapper
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowWithEpisodesEntity
import com.caesar84mx.tvmaze.commons.kotlinextensions.map

class ShowWithEpisodesEntityToShowMapper: Mapper<ShowWithEpisodesEntity, Show> {
    override fun map(t: ShowWithEpisodesEntity): Show = with(t) {
        Show(
            id = showEntity.id,
            serverId = showEntity.serverId,
            name = showEntity.name,
            posterUrl = showEntity.posterUrl,
            schedule = showEntity.schedule,
            genres = showEntity.genres,
            summary = showEntity.summary,
            episodes = episodes.map()
        )
    }
}