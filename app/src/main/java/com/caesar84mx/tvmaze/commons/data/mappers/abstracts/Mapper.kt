package com.caesar84mx.tvmaze.commons.data.mappers.abstracts

interface Mapper<T, V> {
    fun map(t: T): V
}