package com.caesar84mx.tvmaze.commons.data.mappers.abstracts

import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.mappers.*
import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData
import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowWithEpisodesEntity
import com.caesar84mx.tvmaze.details_screen.data.model.EpisodeItemUI
import com.caesar84mx.tvmaze.main_screen.data.model.ShowsListItemUI

@Suppress("UNCHECKED_CAST")
class MapperFactory {
    companion object {
        @JvmStatic
        var dictionary =
            getDictionaryMappers()

        inline fun <reified T, reified V> map(objectToMap: T): V {
            return getMapper<T, V>()
                .map(objectToMap)
        }

        inline fun <reified T, reified V> getMapper(): Mapper<T, V> {
            require(
                dictionary.containsKey(
                    Pair(
                        T::class,
                        V::class))) { "Missing mapper for ${T::class} to ${V::class}" }
            return dictionary[Pair(T::class, V::class)].let { mapper -> mapper as Mapper<T, V> }
        }

        private fun getDictionaryMappers(): Map<Pair<*, *>, Mapper<*, *>> {
            val dictionary = HashMap<Pair<*, *>, Mapper<*, *>>()
            dictionary[Pair(EpisodeRawData::class, EpisodeEntity::class)] = EpisodeRawDataToEpisodeEntityMapper()
            dictionary[Pair(EpisodeEntity::class, Episode::class)] = EpisodeEntityToEpisodeMapper()
            dictionary[Pair(ShowRawData::class, ShowEntity::class)] = ShowRawDataToShowEntityMapper()
            dictionary[Pair(ShowEntity::class, Show::class)] = ShowEntityToShowMapper()
            dictionary[Pair(Show::class, ShowsListItemUI::class)] = ShowToShowListItemUiMapper()
            dictionary[Pair(Episode::class, EpisodeItemUI::class)] = EpisodeToEpisodeUIMapper()
            dictionary[Pair(ShowWithEpisodesEntity::class, Show::class)] = ShowWithEpisodesEntityToShowMapper()

            return dictionary
        }
    }
}