package com.caesar84mx.tvmaze.commons.data.model.api_entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EpisodeRawData(
    @Json(name = "id") var id: Long,
    @Json(name = "name") var name: String,
    @Json(name = "number") var number: Int,
    @Json(name = "season") var season: Int,
    @Json(name = "summary") var summary: String?,
    @Json(name = "image") var image: ImageRawData?
): RawObject