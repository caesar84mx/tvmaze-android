package com.caesar84mx.tvmaze.commons.data.model.api_entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageRawData(
    @Json(name = "medium") val mediumSizePosterUrl: String?,
    @Json(name = "original") val originalSizePosterUrl: String?
)