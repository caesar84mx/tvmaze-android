package com.caesar84mx.tvmaze.commons.data.model.api_entities

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Schedule(
    @Json(name = "time") val time: String,
    @Json(name = "days") val days: List<String>
): Parcelable