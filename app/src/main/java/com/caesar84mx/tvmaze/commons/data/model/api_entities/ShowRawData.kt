package com.caesar84mx.tvmaze.commons.data.model.api_entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ShowRawData(
    @Json(name = "id") val id: Long,
    @Json(name = "name") val name: String,
    @Json(name = "image") val image: ImageRawData?,
    @Json(name = "schedule") val schedule: Schedule,
    @Json(name = "genres") val genres: List<String>,
    @Json(name = "summary") val summary: String
): RawObject