package com.caesar84mx.tvmaze.commons.data.model.persistent_entities

import androidx.room.*
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.ID
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.NAME
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.NUMBER
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.SEASON
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.SERVER_ID
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.SHOW_ID
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity.Companion.TABLE

@Entity(
    tableName = TABLE,
    indices = [
        Index(value = [ID], unique = true),
        Index(value = [SERVER_ID], unique = true),
        Index(value = [SHOW_ID], unique = false),
        Index(value = [NAME], unique = false),
        Index(value = [SEASON], unique = false),
        Index(value = [NUMBER], unique = false)
    ],
    foreignKeys = [
        ForeignKey(
            entity = ShowEntity::class,
            parentColumns = [ShowEntity.ID],
            childColumns = [SHOW_ID],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class EpisodeEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID)
    var id: Long = 0,

    @ColumnInfo(name = SERVER_ID)
    var serverId: Long,

    @ColumnInfo(name = SHOW_ID)
    var showId: Long = 0,

    @ColumnInfo(name = NAME)
    var name: String,

    @ColumnInfo(name = NUMBER)
    var number: Int,

    @ColumnInfo(name = SEASON)
    var season: Int,

    @ColumnInfo(name = SUMMARY)
    var summary: String,

    @ColumnInfo(name = POSTER_URL)
    var posterUrl: String
) {
    companion object {
        const val TABLE = "Episodes"
        const val ID = "id"
        const val SERVER_ID = "server_id"
        const val SHOW_ID = "show_id"
        const val NAME = "name"
        const val NUMBER = "number"
        const val SEASON = "season"
        const val SUMMARY = "summary"
        const val POSTER_URL = "poster_url"
    }
}