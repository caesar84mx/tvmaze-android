package com.caesar84mx.tvmaze.commons.data.model.persistent_entities

import androidx.room.*
import com.caesar84mx.tvmaze.commons.data.model.api_entities.Schedule
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.ID
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.NAME
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.SCHEDULE
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.SERVER_ID
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.SUMMARY
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity.Companion.TABLE

@Entity(
    tableName = TABLE,
    indices = [
        Index(value = [ID], unique = true),
        Index(value = [SERVER_ID], unique = true),
        Index(value = [NAME], unique = false),
        Index(value = [SCHEDULE], unique = false)
    ]
)
data class ShowEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID)
    var id: Long = 0,

    @ColumnInfo(name = SERVER_ID)
    var serverId: Long,

    @ColumnInfo(name = NAME)
    var name: String,

    @ColumnInfo(name = POSTER_URL)
    var posterUrl: String?,

    @ColumnInfo(name = SCHEDULE)
    var schedule: Schedule,

    @ColumnInfo(name = GENRES)
    var genres: List<String>,

    @ColumnInfo(name = SUMMARY)
    var summary: String
) {
    companion object {
        const val TABLE = "Shows"
        const val ID = "id"
        const val SERVER_ID = "server_id"
        const val NAME = "name"
        const val SCHEDULE = "schedule"
        const val GENRES = "genres"
        const val SUMMARY = "summary"
        const val POSTER_URL = "poster_url"
    }
}