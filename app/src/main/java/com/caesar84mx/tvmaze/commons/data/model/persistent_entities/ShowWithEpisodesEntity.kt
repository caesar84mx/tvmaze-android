package com.caesar84mx.tvmaze.commons.data.model.persistent_entities

import androidx.room.Embedded
import androidx.room.Relation

class ShowWithEpisodesEntity {
    @Embedded
    lateinit var showEntity: ShowEntity

    @Relation(
        parentColumn = ShowEntity.ID,
        entityColumn = EpisodeEntity.SHOW_ID
    )
    var episodes: List<EpisodeEntity> = listOf()
}