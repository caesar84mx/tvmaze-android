package com.caesar84mx.tvmaze.commons.data.networking

import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData
import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowsApiClient {
    @GET("/shows")
    fun getShows(): Call<List<ShowRawData>>

    @GET("/shows/{showId}/episodes")
    fun getEpisodes(@Path("showId") showId: Long): Call<List<EpisodeRawData>>
}