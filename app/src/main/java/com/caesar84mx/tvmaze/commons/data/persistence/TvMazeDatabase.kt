package com.caesar84mx.tvmaze.commons.data.persistence

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.caesar84mx.tvmaze.commons.data.converters.ScheduleConverter
import com.caesar84mx.tvmaze.commons.data.converters.StringListConverter
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity
import com.caesar84mx.tvmaze.commons.data.persistence.daos.EpisodesDao
import com.caesar84mx.tvmaze.commons.data.persistence.daos.ShowsDao
import javax.inject.Singleton

@Database(
    entities = [
        ShowEntity::class,
        EpisodeEntity::class
    ],
    version = 1
)
@TypeConverters(
    StringListConverter::class,
    ScheduleConverter::class
)
@Singleton
abstract class TvMazeDatabase : RoomDatabase() {
    abstract fun episodesDao(): EpisodesDao
    abstract fun showsDao(): ShowsDao

    companion object {
        fun getDatabase(context: Context): TvMazeDatabase = Room
            .databaseBuilder(context, TvMazeDatabase::class.java, "tvmaze_database")
            .build()
    }
}