package com.caesar84mx.tvmaze.commons.data.persistence.daos

import androidx.room.*
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity

@Dao
abstract class EpisodesDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun save(episodeEntity: EpisodeEntity)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun save(bulk: List<EpisodeEntity>)

    @Transaction
    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(episodeEntity: EpisodeEntity)

    @Query(
        "SELECT " +
            EpisodeEntity.ID + ", " +
            EpisodeEntity.SERVER_ID + ", " +
            EpisodeEntity.SHOW_ID + ", " +
            EpisodeEntity.NAME + ", " +
            EpisodeEntity.NUMBER + ", " +
            EpisodeEntity.SEASON + ", " +
            EpisodeEntity.SUMMARY + ", " +
            EpisodeEntity.POSTER_URL +
            " FROM " + EpisodeEntity.TABLE +
            " WHERE " + EpisodeEntity.ID +
            " = :id"
    )
    abstract fun get(id: Long): EpisodeEntity?

    @Query(
        "SELECT * FROM " + EpisodeEntity.TABLE +
                " WHERE " + EpisodeEntity.SHOW_ID +
                " = :showId"
    )
    abstract fun findByShow(showId: Long): List<EpisodeEntity>

    @Query(
        "SELECT " +
                EpisodeEntity.ID + ", " +
                EpisodeEntity.SERVER_ID + ", " +
                EpisodeEntity.SHOW_ID + ", " +
                EpisodeEntity.NAME + ", " +
                EpisodeEntity.NUMBER + ", " +
                EpisodeEntity.SEASON + ", " +
                EpisodeEntity.SUMMARY + ", " +
                EpisodeEntity.POSTER_URL +
                " FROM " + EpisodeEntity.TABLE
    )
    abstract fun getAll(): List<EpisodeEntity>

    @Transaction
    @Query("DELETE FROM " + EpisodeEntity.TABLE + " WHERE " + EpisodeEntity.ID + " = :id")
    abstract fun delete(id: Long)

    @Transaction
    @Query("DELETE FROM " + EpisodeEntity.TABLE)
    abstract fun deleteAll()
}