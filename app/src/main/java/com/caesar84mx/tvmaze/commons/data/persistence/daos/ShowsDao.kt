package com.caesar84mx.tvmaze.commons.data.persistence.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowWithEpisodesEntity

@Dao
abstract class ShowsDao {
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun save(showEntity: ShowEntity)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun save(showEntities: List<ShowEntity>)

    @Transaction
    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(showEntity: ShowEntity)

    @Query("SELECT COUNT(" + ShowEntity.ID + ") FROM " + ShowEntity.TABLE)
    abstract fun countShows(): Long

    @Query(
        "SELECT COUNT(" + ShowEntity.ID + ") FROM " +
                ShowEntity.TABLE +
                " WHERE " + ShowEntity.NAME +
                " LIKE '%' || :name || '%'")
    abstract fun countNamed(name: String): Long

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE + " WHERE " +
                ShowEntity.ID + " = :id"
    )
    abstract fun get(id: Long): ShowWithEpisodesEntity?

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE + " WHERE " +
                ShowEntity.SERVER_ID + " = :serverId"
    )
    abstract fun getByServerId(serverId: Long): ShowWithEpisodesEntity?

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE + " WHERE " +
                ShowEntity.ID + " LIKE '%'||:name||'%'"

    )
    abstract fun searchByName(name: String): List<ShowWithEpisodesEntity>

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE + " WHERE " +
                ShowEntity.NAME + " LIKE '%'||:name|| '%'" +
                " LIMIT :limit OFFSET :offset"

    )
    abstract fun getFilteredByNameLive(name: String, limit: Int, offset: Int): LiveData<List<ShowWithEpisodesEntity>>

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE + " LIMIT :limit OFFSET :offset"
    )
    abstract fun getAllLive(limit: Int, offset: Int): LiveData<List<ShowWithEpisodesEntity>>

    @Transaction
    @Query(
        "SELECT " +
                ShowEntity.ID + ", " +
                ShowEntity.SERVER_ID + ", " +
                ShowEntity.NAME + ", " +
                ShowEntity.POSTER_URL + ", " +
                ShowEntity.SCHEDULE + ", " +
                ShowEntity.GENRES + ", " +
                ShowEntity.SUMMARY + " FROM " +
                ShowEntity.TABLE
    )
    abstract fun getAll(): List<ShowWithEpisodesEntity>

    @Transaction
    @Query(
        "DELETE FROM " +
                ShowEntity.TABLE + " WHERE " +
                ShowEntity.ID + " = :id "
    )
    abstract fun delete(id: Long)

    @Transaction
    @Query("DELETE FROM " + ShowEntity.TABLE)
    abstract fun deleteAll()
}