package com.caesar84mx.tvmaze.commons.data.persistence.repos

import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity
import com.caesar84mx.tvmaze.commons.data.persistence.daos.EpisodesDao
import com.caesar84mx.tvmaze.commons.kotlinextensions.map
import javax.inject.Inject

class CommonEpisodesRepository @Inject constructor(
    private val dao: EpisodesDao
): EpisodesRepository {
    override fun getEpisodes(showId: Long): List<Episode> {
        val entities = dao.findByShow(showId)
        return entities.map()
    }

    override fun save(bulk: List<EpisodeRawData>, showId: Long) {
        val entity: List<EpisodeEntity> = bulk.map()
        entity.map { episode ->
            episode.showId = showId
            dao.save(episode)
        }
    }

    override fun save(rawData: EpisodeRawData) {
        val entity: EpisodeEntity = rawData.map()
        dao.save(entity)
    }

    override fun save(bulk: List<EpisodeRawData>) {
        val entities: List<EpisodeEntity> = bulk.map()
        dao.save(entities)
    }

    override suspend fun get(id: Long): Episode? = dao.get(id).map()

    override fun getAll(): List<Episode> = dao.getAll().map()

    override fun delete(id: Long) = dao.delete(id)

    override fun deleteAll() = dao.deleteAll()
}