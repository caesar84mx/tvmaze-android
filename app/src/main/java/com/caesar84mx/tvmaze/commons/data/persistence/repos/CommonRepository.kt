package com.caesar84mx.tvmaze.commons.data.persistence.repos

import com.caesar84mx.tvmaze.commons.data.backbone.BackboneObject
import com.caesar84mx.tvmaze.commons.data.model.api_entities.RawObject

interface CommonRepository<T: BackboneObject, V: RawObject> {
    fun save(rawData: V)
    fun save(bulk: List<V>)
    suspend fun get(id: Long): T?
    fun getAll(): List<T>
    fun delete(id: Long)
    fun deleteAll()
}