package com.caesar84mx.tvmaze.commons.data.persistence.repos

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.EpisodeEntity
import com.caesar84mx.tvmaze.commons.data.model.persistent_entities.ShowEntity
import com.caesar84mx.tvmaze.commons.data.persistence.daos.ShowsDao
import com.caesar84mx.tvmaze.commons.kotlinextensions.launchIO
import com.caesar84mx.tvmaze.commons.kotlinextensions.map
import com.caesar84mx.tvmaze.commons.kotlinextensions.notNull
import javax.inject.Inject
import kotlin.math.ceil

class CommonShowRepository @Inject constructor(
    private val dao: ShowsDao
) : ShowRepository {
    override fun save(rawData: ShowRawData) {
        launchIO {
            val entity: ShowEntity = rawData.map()
            dao.save(entity)
        }
    }

    override fun save(bulk: List<ShowRawData>) {
        launchIO {
            val entities: List<ShowEntity> = bulk.map()
            dao.save(entities)
        }
    }

    override suspend fun get(id: Long): Show? {
        val intermediate = dao.get(id)
        val show: Show = intermediate?.showEntity.map()
        val episodes: List<Episode> = intermediate?.episodes
            .notNull()
            .map { entity -> entity.map<EpisodeEntity, Episode>() }

        show.episodes = episodes

        return show
    }

    override fun filterByName(name: String): List<Show> {
        val intermediate = dao.searchByName(name)
        val shows: List<Show> = intermediate.map { it.showEntity }.map()
        val episodes: List<Episode> = intermediate.flatMap { it.episodes }.map()

        shows.map { show ->
            show.episodes = episodes.filter { episode -> episode.showId == show.id }
        }

        return shows
    }

    override fun filterByNameLive(name: String, limit: Int, page: Int): LiveData<List<Show>> {
        return Transformations.map(
            dao.getFilteredByNameLive(
                name,
                limit,
                getOffset(page, limit)
            )
        ) { intermediate ->
            val showEntities: List<ShowEntity> = intermediate.map { it.showEntity }
            val shows: List<Show> = showEntities.map()

            val episodeEntities: List<EpisodeEntity> = intermediate.flatMap { it.episodes }
            val episodes: List<Episode> = episodeEntities.map()

            shows.map { show ->
                show.episodes = episodes.filter { episode -> episode.showId == show.id }
            }

            return@map shows
        }
    }

    override fun getAll(): List<Show> {
        val intermediate = dao.getAll()
        val shows: List<Show> = intermediate.map { it.showEntity }.map()
        val episodes: List<Episode> = intermediate.map { it.episodes }.map()
        shows.map { show ->
            show.episodes = episodes.filter { episode -> episode.showId == show.id }
        }

        return shows
    }

    override fun getAllLive(limit: Int, page: Int): LiveData<List<Show>> {
        return Transformations.map(dao.getAllLive(limit, getOffset(page, limit))) { intermediate ->
            val showEntities: List<ShowEntity> = intermediate.map { it.showEntity }
            val shows: List<Show> = showEntities.map()

            val episodeEntities: List<EpisodeEntity> = intermediate.flatMap { it.episodes }
            val episodes: List<Episode> = episodeEntities.map()

            shows.map { show ->
                show.episodes = episodes.filter { episode -> episode.showId == show.id }
            }

            return@map shows
        }
    }

    override fun findByServerId(serverId: Long): Show? = dao.getByServerId(serverId).map()

    override fun delete(id: Long) = dao.delete(id)

    override fun deleteAll() = dao.deleteAll()

    override fun countPages(pageLimit: Int): Int = ceil(dao.countShows().toDouble() / pageLimit).toInt()

    override fun countFilteredPages(name: String, pageLimit: Int): Int = ceil(dao.countNamed(name).toDouble() / pageLimit).toInt()

    private fun getOffset(page: Int, itemsPerPage: Int): Int = (page - 1) * itemsPerPage
}