package com.caesar84mx.tvmaze.commons.data.persistence.repos

import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData

interface EpisodesRepository: CommonRepository<Episode, EpisodeRawData> {
    fun getEpisodes(showId: Long): List<Episode>
    fun save(bulk: List<EpisodeRawData>, showId: Long)
}