package com.caesar84mx.tvmaze.commons.data.persistence.repos

import androidx.lifecycle.LiveData
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData

interface ShowRepository: CommonRepository<Show, ShowRawData> {
    fun filterByName(name: String): List<Show>
    fun filterByNameLive(name: String, limit: Int, page: Int): LiveData<List<Show>>
    fun getAllLive(limit: Int, page: Int): LiveData<List<Show>>
    fun findByServerId(serverId: Long): Show?
    fun countPages(pageLimit: Int): Int
    fun countFilteredPages(name: String, pageLimit: Int): Int
}