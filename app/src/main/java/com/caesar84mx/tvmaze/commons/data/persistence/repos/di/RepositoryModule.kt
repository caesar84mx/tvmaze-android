package com.caesar84mx.tvmaze.commons.data.persistence.repos.di

import com.caesar84mx.tvmaze.commons.data.persistence.repos.CommonEpisodesRepository
import com.caesar84mx.tvmaze.commons.data.persistence.repos.CommonShowRepository
import com.caesar84mx.tvmaze.commons.data.persistence.repos.EpisodesRepository
import com.caesar84mx.tvmaze.commons.data.persistence.repos.ShowRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {
    @Singleton
    @Binds
    abstract fun bindShowsRepository(showsRepository: CommonShowRepository): ShowRepository

    @Singleton
    @Binds
    abstract fun bindEpisodeRepository(episodesRepository: CommonEpisodesRepository): EpisodesRepository
}