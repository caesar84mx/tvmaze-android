package com.caesar84mx.tvmaze.commons.data.uidata

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewholder<T: UiHolder>(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bind(viewModel: T)
}