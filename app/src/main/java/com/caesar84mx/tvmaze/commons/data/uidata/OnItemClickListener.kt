package com.caesar84mx.tvmaze.commons.data.uidata

interface OnItemClickListener {
    fun onItemClick(id: Long)
}
