package com.caesar84mx.tvmaze.commons.di

import com.caesar84mx.tvmaze.commons.MazeTvApp
import com.caesar84mx.tvmaze.commons.data.di.ActivityBuilder
import com.caesar84mx.tvmaze.commons.data.di.ApiModule
import com.caesar84mx.tvmaze.commons.data.di.AppModule
import com.caesar84mx.tvmaze.commons.data.di.DatabaseModule
import com.caesar84mx.tvmaze.commons.data.persistence.repos.di.RepositoryModule
import com.caesar84mx.tvmaze.main_screen.di.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidInjectionModule::class,
        ApiModule::class,
        DatabaseModule::class,
        RepositoryModule::class,
        ActivityBuilder::class,
        MainActivityModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: MazeTvApp): Builder
        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: MazeTvApp)
}