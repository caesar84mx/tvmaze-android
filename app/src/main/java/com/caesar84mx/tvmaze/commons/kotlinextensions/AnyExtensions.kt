package com.caesar84mx.tvmaze.commons.kotlinextensions

import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.MapperFactory
import com.google.gson.Gson

fun Any.toJson(): String {
    val gson = Gson()
    return gson.toJson(this)
}

inline fun <reified T, reified V> T.map(): V {
    return MapperFactory.map(this)
}