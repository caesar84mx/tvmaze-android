package com.caesar84mx.tvmaze.commons.kotlinextensions

import com.caesar84mx.tvmaze.commons.data.mappers.abstracts.MapperFactory

inline fun <reified T: Any> List<T>?.notNull() = this ?: listOf()

inline fun <reified T, reified V> List<T>?.map(): List<V> =
    if (this != null) with(MapperFactory.getMapper<T, V>()) { map { map(it) } } else listOf()