package com.caesar84mx.tvmaze.details_screen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.details_screen.data.model.EpisodeItemUI
import com.caesar84mx.tvmaze.details_screen.viewholders.EpisodeItemViewholder

class EpisodesAdapter(
    private val data: List<EpisodeItemUI>,
    private val onItemClickListener: OnItemClickListener
): RecyclerView.Adapter<EpisodeItemViewholder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeItemViewholder {
        val inflater = LayoutInflater.from(parent.context)
        return EpisodeItemViewholder(inflater.inflate(R.layout.row_episodes_list, parent, false), onItemClickListener)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: EpisodeItemViewholder, position: Int) {
        holder.bind(data[position])
    }
}