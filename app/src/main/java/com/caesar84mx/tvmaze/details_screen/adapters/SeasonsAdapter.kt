package com.caesar84mx.tvmaze.details_screen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.details_screen.data.model.SeasonItemUI
import com.caesar84mx.tvmaze.details_screen.viewholders.SeasonsViewholder

class SeasonsAdapter(
    private val data: List<SeasonItemUI>,
    private val onEpisodeClickListener: OnItemClickListener
): RecyclerView.Adapter<SeasonsViewholder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonsViewholder {
        val inflater = LayoutInflater.from(parent.context)
        return SeasonsViewholder(inflater.inflate(R.layout.row_seasons_list, parent, false), onEpisodeClickListener)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: SeasonsViewholder, position: Int) {
        holder.bind(data[position])
    }
}