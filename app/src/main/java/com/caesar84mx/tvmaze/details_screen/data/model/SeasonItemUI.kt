package com.caesar84mx.tvmaze.details_screen.data.model

import com.caesar84mx.tvmaze.commons.data.uidata.UiHolder

data class SeasonItemUI(
    val seasonNumber: Int,
    var episodes: List<EpisodeItemUI>
): UiHolder