package com.caesar84mx.tvmaze.details_screen.data.model

import com.caesar84mx.tvmaze.commons.data.uidata.UiHolder

data class ShowDetailUI(
    val title: String,
    val posterUrl: String,
    val daysOnAir: String,
    val timeOnAir: String,
    val genres: String,
    val summary: String,
    val seasons: List<SeasonItemUI>
): UiHolder