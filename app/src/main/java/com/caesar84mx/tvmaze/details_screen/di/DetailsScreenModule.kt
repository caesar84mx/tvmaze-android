package com.caesar84mx.tvmaze.details_screen.di

import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel
import com.caesar84mx.tvmaze.details_screen.viewmodels.ShowDetailsCommonViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class DetailsScreenModule {
    @Binds
    abstract fun bindShowDetailsViewModel(showDetailsCommonViewModel: ShowDetailsCommonViewModel): AbstractShowDetailsViewModel
}