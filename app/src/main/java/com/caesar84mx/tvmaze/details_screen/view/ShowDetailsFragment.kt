package com.caesar84mx.tvmaze.details_screen.view

import android.content.Context
import android.os.Bundle
import android.text.Html
import android.text.Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.commons.kotlinextensions.notTull
import com.caesar84mx.tvmaze.commons.kotlinextensions.observe
import com.caesar84mx.tvmaze.commons.kotlinextensions.withViewModel
import com.caesar84mx.tvmaze.details_screen.adapters.SeasonsAdapter
import com.caesar84mx.tvmaze.details_screen.data.model.SeasonItemUI
import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel
import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel.Companion.getSeasons
import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel.EpisodeResponse
import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel.EpisodeResponse.Failure
import com.caesar84mx.tvmaze.details_screen.viewmodels.AbstractShowDetailsViewModel.EpisodeResponse.Success
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ShowDetailsFragment: Fragment(), OnItemClickListener {
    @Inject
    lateinit var viewModel: AbstractShowDetailsViewModel

    private lateinit var rootView: View
    private lateinit var ivPoster: ImageView
    private lateinit var tvTitle: TextView
    private lateinit var tvGenres: TextView
    private lateinit var tvOnAir: TextView
    private lateinit var tvTime: TextView
    private lateinit var tvSummary: TextView
    private lateinit var tvNoEpisodesInfo: TextView
    private lateinit var rvSeasons: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this) {
                findNavController().navigate(R.id.actionMoveToMainFragment)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_show_details, container, false)

        ivPoster = rootView.findViewById(R.id.ivPoster)
        tvTitle = rootView.findViewById(R.id.tvTitle)
        tvGenres = rootView.findViewById(R.id.tvGenres)
        tvOnAir = rootView.findViewById(R.id.tvOnAir)
        tvTime = rootView.findViewById(R.id.tvTime)
        tvSummary = rootView.findViewById(R.id.tvSummary)
        tvNoEpisodesInfo = rootView.findViewById(R.id.tvLoadingSeasons)
        rvSeasons = rootView.findViewById(R.id.rvSeasons)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        withViewModel({ viewModel }) {
            observe(episodesLiveData, ::onGotResponse)
            observe(episodeClickedLiveData) { episode ->
                val bundle = bundleOf("episode" to episode)
                findNavController().navigate(R.id.actionMoveToEpisodeDetailsFragment, bundle)
            }
        }

        arguments?.let { args ->
            val show = ShowDetailsFragmentArgs.fromBundle(args).show
            setDetails(show)
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onItemClick(id: Long) {
        viewModel.onEpisodeClicked(id)
    }

    private fun onGotResponse(response: EpisodeResponse) {
        when(response) {
            is Success -> {
                setSeasons(response.data)
            }
            is Failure -> {
                rvSeasons.visibility = View.GONE
                tvNoEpisodesInfo.visibility = View.VISIBLE
                tvNoEpisodesInfo.text = getString(R.string.failed_loading_episodes)
            }
        }
    }

    private fun setDetails(details: Show) {
        if (details.posterUrl.notTull().isNotEmpty()) {
            Picasso.get().load(details.posterUrl).into(ivPoster)
        }

        tvTitle.text = details.name
        tvGenres.text = details.genres.joinToString()
        tvOnAir.text = details.schedule.days.joinToString()
        tvTime.text = if (details.schedule.time.isNotEmpty()) {
            details.schedule.time
        } else {
            "No info"
        }
        tvSummary.text = Html.fromHtml(details.summary, TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)

        if (details.episodes.isNotEmpty()) {
            setSeasons(getSeasons(details.episodes))
        } else {
            viewModel.lookForEpisodes(details.serverId)
        }
    }

    private fun setSeasons(seasons: List<SeasonItemUI>) {
        tvNoEpisodesInfo.visibility = View.GONE

        rvSeasons.apply {
            visibility = View.VISIBLE

            layoutManager = LinearLayoutManager(requireContext())
            adapter = SeasonsAdapter(seasons, this@ShowDetailsFragment)
        }
    }
}
