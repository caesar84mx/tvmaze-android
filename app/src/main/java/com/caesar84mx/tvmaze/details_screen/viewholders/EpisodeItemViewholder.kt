package com.caesar84mx.tvmaze.details_screen.viewholders

import android.view.View
import android.widget.ImageView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.uidata.BaseViewholder
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.commons.kotlinextensions.onClick
import com.caesar84mx.tvmaze.details_screen.data.model.EpisodeItemUI
import com.squareup.picasso.Picasso

class EpisodeItemViewholder(
    itemView: View,
    private val onClickListener: OnItemClickListener
) : BaseViewholder<EpisodeItemUI>(itemView) {
    private val ivPoster: ImageView = itemView.findViewById(R.id.ivPoster)

    override fun bind(viewModel: EpisodeItemUI) {
        if (viewModel.posterUrl.isNotEmpty()) {
            Picasso.get().load(viewModel.posterUrl).into(ivPoster)
        }

        itemView.onClick { onClickListener.onItemClick(viewModel.id) }
    }
}