package com.caesar84mx.tvmaze.details_screen.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.uidata.BaseViewholder
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.details_screen.adapters.EpisodesAdapter
import com.caesar84mx.tvmaze.details_screen.data.model.SeasonItemUI

class SeasonsViewholder(
    itemView: View,
    private val onEpisodeClickListener: OnItemClickListener
) : BaseViewholder<SeasonItemUI>(itemView) {
    private val tvTitle: TextView = itemView.findViewById(R.id.tvSeasonTitle)
    private val rvEpisodes: RecyclerView = itemView.findViewById(R.id.rvEpisodes)
    
    override fun bind(viewModel: SeasonItemUI) {
        val context = itemView.context
        val format = context.getString(R.string.season_num_format)
        tvTitle.text = String.format(format, viewModel.seasonNumber)
        rvEpisodes.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = EpisodesAdapter(viewModel.episodes, onEpisodeClickListener)
            isNestedScrollingEnabled = false
        }
    }
}