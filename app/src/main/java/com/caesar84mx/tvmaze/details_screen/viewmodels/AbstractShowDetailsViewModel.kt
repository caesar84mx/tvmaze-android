package com.caesar84mx.tvmaze.details_screen.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.commons.kotlinextensions.map
import com.caesar84mx.tvmaze.details_screen.data.model.SeasonItemUI

abstract class AbstractShowDetailsViewModel: ViewModel() {
    val episodesLiveData: MutableLiveData<EpisodeResponse> = MutableLiveData()
    val episodeClickedLiveData: MutableLiveData<Episode> = MutableLiveData()

    abstract fun lookForEpisodes(showServerId: Long)

    abstract fun onEpisodeClicked(id: Long)

    companion object {
        fun getSeasons(episodes: List<Episode>) = episodes
            .groupBy { it.season }
            .map { seasonPair -> SeasonItemUI(seasonPair.key, seasonPair.value.map()) }
    }

    sealed class EpisodeResponse {
        data class Failure(val throwable: Throwable): EpisodeResponse()
        data class Success(val data: List<SeasonItemUI>): EpisodeResponse()
    }
}
