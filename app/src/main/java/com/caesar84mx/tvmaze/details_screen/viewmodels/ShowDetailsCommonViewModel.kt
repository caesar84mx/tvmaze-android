package com.caesar84mx.tvmaze.details_screen.viewmodels

import com.caesar84mx.tvmaze.commons.data.model.api_entities.EpisodeRawData
import com.caesar84mx.tvmaze.commons.data.networking.ShowsApiClient
import com.caesar84mx.tvmaze.commons.data.persistence.repos.EpisodesRepository
import com.caesar84mx.tvmaze.commons.data.persistence.repos.ShowRepository
import com.caesar84mx.tvmaze.commons.kotlinextensions.launchIO
import com.caesar84mx.tvmaze.commons.kotlinextensions.notNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ShowDetailsCommonViewModel @Inject constructor(
    private val episodesRepository: EpisodesRepository,
    private val showsRepository: ShowRepository,
    private val apiClient: ShowsApiClient
) : AbstractShowDetailsViewModel() {
    override fun lookForEpisodes(showServerId: Long) {
        launchIO {
            showsRepository.findByServerId(showServerId)?.let { relatedShow ->
                var episodes = episodesRepository.getEpisodes(relatedShow.id)

                if (episodes.isEmpty()) {
                    apiClient.getEpisodes(showServerId).enqueue(object: Callback<List<EpisodeRawData>> {
                        override fun onFailure(call: Call<List<EpisodeRawData>>, t: Throwable) {
                            episodesLiveData.postValue(EpisodeResponse.Failure(t))
                        }

                        override fun onResponse(
                            call: Call<List<EpisodeRawData>>,
                            response: Response<List<EpisodeRawData>>
                        ) {
                            if (response.isSuccessful) {
                                launchIO {
                                    episodesRepository.save(response.body().notNull(), relatedShow.id)
                                    episodes = episodesRepository.getEpisodes(relatedShow.id)
                                    episodesLiveData.postValue(EpisodeResponse.Success(getSeasons(episodes)))
                                }
                            }
                        }
                    })
                } else {
                    episodesLiveData.postValue(EpisodeResponse.Success(getSeasons(episodes)))
                }
            }
        }
    }

    override fun onEpisodeClicked(id: Long) {
        launchIO {
            episodesRepository.get(id)?.let { episode ->
                episodeClickedLiveData.postValue(episode)
            }
        }
    }
}