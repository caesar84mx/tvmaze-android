package com.caesar84mx.tvmaze.episode_details.view

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.backbone.Episode
import com.caesar84mx.tvmaze.main_screen.view.MainActivity
import com.squareup.picasso.Picasso

class EpisodeDetailsFragment: Fragment() {
    private lateinit var rootView: View
    private lateinit var tvTitle: TextView
    private lateinit var tvSeasonAndEpisode: TextView
    private lateinit var ivPoster: ImageView
    private lateinit var tvSummary: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this) {
                (requireActivity() as MainActivity).lastClickedShow?.let { show ->
                    val bundle = bundleOf("show" to show)
                    findNavController().navigate(R.id.actionMoveToShowDetailsFragment, bundle)
                }
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_episode_details, container, false)

        tvTitle = rootView.findViewById(R.id.tvTitle)
        tvSeasonAndEpisode = rootView.findViewById(R.id.tvSeasonAndEpisode)
        ivPoster = rootView.findViewById(R.id.ivPoster)
        tvSummary = rootView.findViewById(R.id.tvSummary)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            val episode = EpisodeDetailsFragmentArgs.fromBundle(args).episode
            setDetails(episode)
        }
    }

    private fun setDetails(episode: Episode) {
        tvTitle.text = episode.name

        val format = getString(R.string.season_and_episode_format)
        tvSeasonAndEpisode.text = String.format(format, episode.season, episode.number)

        if (!episode.posterUrl.isNullOrEmpty()) {
            Picasso.get().load(episode.posterUrl).into(ivPoster)
        }

        tvSummary.text = Html.fromHtml(episode.summary, Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)
    }
}