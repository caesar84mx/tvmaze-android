package com.caesar84mx.tvmaze.main_screen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.main_screen.data.model.ShowsListItemUI
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.main_screen.viewholders.ShowsListItemViewHolder

class ShowsAdapter(
    private val data: MutableList<ShowsListItemUI>,
    private val onClickListener: OnItemClickListener
): RecyclerView.Adapter<ShowsListItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowsListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ShowsListItemViewHolder(
            inflater.inflate(R.layout.row_shows_list, parent, false),
            onClickListener
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ShowsListItemViewHolder, position: Int) {
        holder.bind(data[position])
    }
}