package com.caesar84mx.tvmaze.main_screen.data.model

import com.caesar84mx.tvmaze.commons.data.uidata.UiHolder

data class ShowsListItemUI(
    val id: Long,
    val title: String,
    val posterUrl: String
): UiHolder