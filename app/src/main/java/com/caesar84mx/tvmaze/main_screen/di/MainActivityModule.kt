package com.caesar84mx.tvmaze.main_screen.di

import com.caesar84mx.tvmaze.details_screen.di.DetailsScreenModule
import com.caesar84mx.tvmaze.details_screen.view.ShowDetailsFragment
import com.caesar84mx.tvmaze.main_screen.view.MainScreenFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [MainScreenModule::class])
    abstract fun provideMainScreenFragment(): MainScreenFragment

    @ContributesAndroidInjector(modules = [DetailsScreenModule::class])
    abstract fun provideShowDetailsFragment(): ShowDetailsFragment
}