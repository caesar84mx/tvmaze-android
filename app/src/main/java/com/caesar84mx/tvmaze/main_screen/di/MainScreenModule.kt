package com.caesar84mx.tvmaze.main_screen.di

import com.caesar84mx.tvmaze.main_screen.usecases.AbstractShowsUsecase
import com.caesar84mx.tvmaze.main_screen.usecases.ShowsUsecase
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenCommonViewModel
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class MainScreenModule {
    @Binds
    abstract fun bindMainScreenViewModel(mainScreenViewModel: MainScreenCommonViewModel): MainScreenViewModel

    @Binds
    abstract fun bindAbstractShowsUsecase(showsUsecase: ShowsUsecase): AbstractShowsUsecase
}