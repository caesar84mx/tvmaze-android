package com.caesar84mx.tvmaze.main_screen.usecases

abstract class AbstractShowsUsecase {
    abstract fun refreshShows(onFailure: (Throwable) -> Unit)
}