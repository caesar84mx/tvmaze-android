package com.caesar84mx.tvmaze.main_screen.usecases

import com.caesar84mx.tvmaze.commons.data.model.api_entities.ShowRawData
import com.caesar84mx.tvmaze.commons.data.networking.ShowsApiClient
import com.caesar84mx.tvmaze.commons.data.persistence.repos.ShowRepository
import com.caesar84mx.tvmaze.commons.kotlinextensions.launchIO
import com.caesar84mx.tvmaze.commons.kotlinextensions.notNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ShowsUsecase @Inject constructor(
    private val showsApiClient: ShowsApiClient,
    private val showsRepository: ShowRepository
) : AbstractShowsUsecase() {
    override fun refreshShows(onFailure: (Throwable) -> Unit) {
        showsApiClient.getShows().enqueue(object: Callback<List<ShowRawData>> {
            override fun onFailure(call: Call<List<ShowRawData>>, t: Throwable) {
                onFailure(t)
            }

            override fun onResponse(call: Call<List<ShowRawData>>, response: Response<List<ShowRawData>>) {
                launchIO {
                    showsRepository.deleteAll()
                    showsRepository.save(response.body().notNull())
                }
            }
        })
    }
}