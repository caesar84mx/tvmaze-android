package com.caesar84mx.tvmaze.main_screen.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.commons.kotlinextensions.observe
import com.caesar84mx.tvmaze.commons.kotlinextensions.onClick
import com.caesar84mx.tvmaze.commons.kotlinextensions.onTextChanged
import com.caesar84mx.tvmaze.commons.kotlinextensions.withViewModel
import com.caesar84mx.tvmaze.main_screen.adapters.ShowsAdapter
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenViewModel
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenViewModel.PagingBarStatus
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenViewModel.ShowItemClickedEvent
import com.caesar84mx.tvmaze.main_screen.viewmodels.MainScreenViewModel.DataStatus.*
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class MainScreenFragment : Fragment(),
    OnItemClickListener, OnRefreshListener {
    @Inject
    lateinit var viewModel: MainScreenViewModel

    private lateinit var rootView: View
    private lateinit var searchField: EditText
    private lateinit var showsListView: RecyclerView
    private lateinit var emptyListText: TextView
    private lateinit var swipeLayout: SwipeRefreshLayout
    private lateinit var previousButton: Button
    private lateinit var nextButton: Button
    private lateinit var flPagingBar: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this) {
                requireActivity().finish()
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_main_screen, container, false)

        searchField = rootView.findViewById(R.id.etSearchBar)
        showsListView = rootView.findViewById(R.id.rvShows)
        emptyListText = rootView.findViewById(R.id.tvEmptyList)
        swipeLayout = rootView.findViewById(R.id.slSwipeLayout)
        previousButton = rootView.findViewById(R.id.btnPrevious)
        nextButton = rootView.findViewById(R.id.btnNext)
        flPagingBar = rootView.findViewById(R.id.flBar)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        withViewModel({ viewModel }) {
            observe(dataStatus, ::onDataStatusChanged)
            observe(pagingStatus, ::onPagingStatusChanged)
            observe(clickedEvent) { event ->
                (requireActivity() as MainActivity).lastClickedShow = event.show
                navigateToDetails(event.show)
            }
        }

        setupListeners()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onItemClick(id: Long) {
        viewModel.onItemClicked(id)
    }

    override fun onRefresh() {
        viewModel.onRefresh()
    }

    private fun onDataStatusChanged(status: MainScreenViewModel.DataStatus) {
        when(status) {
            is Loading -> {
                showsListView.visibility = View.GONE
                emptyListText.visibility = View.GONE
                swipeLayout.isRefreshing = true
            }
            is Loaded -> {
                swipeLayout.isRefreshing = false

                if (status.data.isEmpty()) {
                    showsListView.visibility = View.GONE
                    emptyListText.visibility = View.VISIBLE
                } else {
                    emptyListText.visibility = View.GONE
                    flPagingBar.visibility = View.VISIBLE

                    showsListView.apply {
                        visibility = View.VISIBLE
                        layoutManager = LinearLayoutManager(requireContext())
                        val showsAdapter = ShowsAdapter(status.data.toMutableList(),this@MainScreenFragment)
                        adapter = showsAdapter
                        isNestedScrollingEnabled = false
                    }
                }
            }
            is Error -> {
                swipeLayout.isRefreshing = false
                showsListView.visibility = View.GONE
                emptyListText.visibility = View.VISIBLE

                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.error_title))
                    .setMessage(status.msg)
                    .setPositiveButton(getString(R.string.ok_button_text), null)
                    .show()
            }
        }
    }

    private fun onPagingStatusChanged(status: PagingBarStatus) {
        if (status.isBarShown.not()) {
            flPagingBar.visibility = View.GONE
        } else {
            flPagingBar.visibility = View.VISIBLE

            if (status.isNextEnabled) {
                nextButton.visibility = View.VISIBLE
            } else {
                nextButton.visibility = View.GONE
            }

            if (status.isPreviousEnabled) {
                previousButton.visibility = View.VISIBLE
            } else {
                previousButton.visibility = View.GONE
            }
        }
    }

    private fun setupListeners() {
        swipeLayout.setOnRefreshListener(this)
        previousButton.onClick { viewModel.onPrevClicked() }
        nextButton.onClick { viewModel.onNextClicked() }
        searchField.onTextChanged { text ->
            viewModel.onNameQueryChanged(text)
        }
    }

    private fun navigateToDetails(show: Show) {
        val bundle = bundleOf("show" to show)
        findNavController().navigate(R.id.actionMoveToShowDetailsFragment, bundle)
    }
}
