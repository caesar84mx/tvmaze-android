package com.caesar84mx.tvmaze.main_screen.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.caesar84mx.tvmaze.R
import com.caesar84mx.tvmaze.commons.data.uidata.BaseViewholder
import com.caesar84mx.tvmaze.commons.data.uidata.OnItemClickListener
import com.caesar84mx.tvmaze.commons.kotlinextensions.onClick
import com.caesar84mx.tvmaze.main_screen.data.model.ShowsListItemUI
import com.squareup.picasso.Picasso

class ShowsListItemViewHolder(
    itemView: View,
    private val onItemClickListener: OnItemClickListener
): BaseViewholder<ShowsListItemUI>(itemView) {
    private val ivPoster: ImageView = itemView.findViewById(R.id.ivPoster)
    private val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
    private val container: LinearLayout = itemView.findViewById(R.id.llRowContainer)

    override fun bind(viewModel: ShowsListItemUI) {
        if (viewModel.posterUrl.isNotEmpty()) {
            Picasso.get().load(viewModel.posterUrl).into(ivPoster)
        }

        tvTitle.text = viewModel.title

        container.onClick { onItemClickListener.onItemClick(viewModel.id) }
    }

}