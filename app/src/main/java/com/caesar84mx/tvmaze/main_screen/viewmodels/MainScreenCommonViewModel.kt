package com.caesar84mx.tvmaze.main_screen.viewmodels

import com.caesar84mx.tvmaze.commons.data.Constants
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.commons.data.persistence.repos.ShowRepository
import com.caesar84mx.tvmaze.commons.kotlinextensions.launchIO
import com.caesar84mx.tvmaze.commons.kotlinextensions.map
import com.caesar84mx.tvmaze.commons.kotlinextensions.notTull
import com.caesar84mx.tvmaze.main_screen.usecases.AbstractShowsUsecase
import javax.inject.Inject

class MainScreenCommonViewModel @Inject constructor(
    private val usecase: AbstractShowsUsecase,
    private val showsRepository: ShowRepository
): MainScreenViewModel() {
    private var pages: Int = 0
    private var currentPage: Int = 1
    private val pageSize = Constants.Shows.PAGE_LIMIT
    private var searchByNameQuery: String = ""
    private var dataSource = showsRepository.filterByNameLive(searchByNameQuery, pageSize, 0)

    init {
        dataStatus.addSource(dataSource, ::onShowsDataChanged)
    }

    override fun onNameQueryChanged(nameQuery: String) {
        searchByNameQuery = nameQuery
        updateDataSource()
    }

    override fun onRefresh() {
        dataStatus.postValue(DataStatus.Loading)
        pagingStatus.postValue(PagingBarStatus(
            isBarShown = false,
            isNextEnabled = false,
            isPreviousEnabled = false
        ))
        usecase.refreshShows { error ->
            dataStatus.postValue(DataStatus.Error(error.localizedMessage.notTull()))
        }
    }

    override fun onItemClicked(id: Long) {
        launchIO {
            showsRepository.get(id)?.let {
                clickedEvent.postValue(ShowItemClickedEvent(it))
            }
        }
    }

    override fun onPrevClicked() {
        if (currentPage > 1) {
            --currentPage
            updateDataSource()
        }
    }

    override fun onNextClicked() {
        if (currentPage < pages && pages > 1) {
            ++currentPage
            updateDataSource()
        }
    }

    private fun onShowsDataChanged(data: List<Show>) {
        launchIO {
            pages = when {
                data.isEmpty() -> 0
                data.size <= 10 -> 1
                else -> showsRepository.countFilteredPages(searchByNameQuery, pageSize)
            }

            updatePagingStatus()
            dataStatus.postValue(DataStatus.Loaded(data.map()))
        }
    }

    private fun updatePagingStatus() {
        when {
            pages <= 1 -> {
                pagingStatus.postValue(PagingBarStatus(
                    isBarShown = false,
                    isNextEnabled = false,
                    isPreviousEnabled = false
                ))
            }
            currentPage == 1 -> {
                pagingStatus.postValue(PagingBarStatus(
                    isBarShown = true,
                    isNextEnabled = true,
                    isPreviousEnabled = false))
            }
            currentPage == pages -> {
                pagingStatus.postValue(PagingBarStatus(
                    isBarShown = true,
                    isNextEnabled = false,
                    isPreviousEnabled = true
                ))
            }
            else -> {
                pagingStatus.postValue(PagingBarStatus(
                    isBarShown = true,
                    isNextEnabled = true,
                    isPreviousEnabled = true
                ))
            }
        }
    }

    private fun updateDataSource() {
        dataStatus.removeSource(dataSource)
        dataSource = showsRepository.filterByNameLive(searchByNameQuery, pageSize, currentPage)
        dataStatus.addSource(dataSource, ::onShowsDataChanged)
    }
}