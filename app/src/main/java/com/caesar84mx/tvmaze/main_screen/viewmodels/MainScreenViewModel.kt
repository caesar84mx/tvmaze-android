package com.caesar84mx.tvmaze.main_screen.viewmodels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.caesar84mx.tvmaze.commons.data.backbone.Show
import com.caesar84mx.tvmaze.main_screen.data.model.ShowsListItemUI

abstract class MainScreenViewModel : ViewModel() {
    val dataStatus: MediatorLiveData<DataStatus> = MediatorLiveData()
    val pagingStatus: MutableLiveData<PagingBarStatus> = MutableLiveData()
    val clickedEvent: MutableLiveData<ShowItemClickedEvent> = MutableLiveData()

    abstract fun onNameQueryChanged(nameQuery: String)

    abstract fun onRefresh()

    abstract fun onItemClicked(id: Long)

    abstract fun onPrevClicked()

    abstract fun onNextClicked()

    sealed class DataStatus {
        object Loading: DataStatus()
        data class Loaded(val data: List<ShowsListItemUI>): DataStatus()
        data class Error(val msg: String): DataStatus()
    }

    data class ShowItemClickedEvent(val show: Show)

    data class PagingBarStatus(
        val isBarShown: Boolean,
        val isNextEnabled: Boolean,
        val isPreviousEnabled: Boolean
    )
}
