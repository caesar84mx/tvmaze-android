# TV Maze Test Application

#### How to install:
1. The .apk file is located in the project root, with the name _maze-tv-test-app-debug.apk_.
2. Copy it to your Android smartphone and install.
3. The minimum supported Android version is Android Nougat.

#### Implemented mandatory features:
1. List all the series from API;
2. Listing shows the name and poster image of the series;
3. Search by name;
4. On list item click the user is redirected to series details
5. In series details, besides the other requested info, the episodes are grouped by seasons;
6. When clicking on an episode, the user is redirected to episode details screen.

#### Not implemented optional features:
* Pin screen;
* TouchId;
* Sort series alphabetically;
* People search and persons details.

Anyway, as soon as the code is highly scalable and well organized, the implementation of these features may take not more than one story point per each.
If tou have any questions or comments, please, don't hesitate to contact me.

#### Contacts:
* email: dymnov.dm@gmail.com
* telegram: @caesar_84
* LinkedIn: https://www.linkedin.com/in/maxim-dymnov-4b0321b2/ 
  
